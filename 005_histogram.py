# Nice Histogram From Text
# Author: Armando Sousa
# License [GPL v3]
# In short, free software: Useful, transparent, no warranty


import matplotlib.pyplot as plt
import numpy as np

# https://codereview.stackexchange.com/questions/200291/histogram-word-counter-in-python
# https://stackoverflow.com/questions/15735406/converting-word-frequency-to-a-graphical-histogram-in-python



"""
Word Counter

Given an body of text, return a hash table of the frequency of each word.
"""



def word_count(sentence):
    """
    Word Counter

    Given an body of text, return a hash table of the frequency of    each word.

    ..  warnings::

        - Capital and lower case versions of the same word should be counted as the same word.

        - Remove punctuations from all words.
    ..  note::


    Where N is the number of characters in the string.

        - Time: O(N)

        - Space: O(N)

    :Example:

    >>> word_count('The cat and the hat.')
    {'the': 2, 'cat': 1, 'and': 1, 'hat': 1}
    >>> word_count('As soon as possible.')
    {'as': 2, 'soon': 1, 'possible': 1}
    >>> word_count("It's a man, it's a plane, it's superman!")
    {'its': 3, 'a': 2, 'man': 1, 'plane': 1, 'superman': 1}

    :param sentence: Input string
    :type sentence: str

    :return: Returns hash-table of frequence of each word in input
    :rtype: dict
    """

    translate = sentence.maketrans({char: None for char in "-'.,:*!)("})
    cleaned_words = sentence.lower().translate(translate).split()
    word_counter = {}
    for word in cleaned_words:
        #if word[-1] == "s" : word = word[0:len(word)-1] # remover plurais
        if word in word_counter:
            word_counter[word] += 1
        else:
            word_counter[word] = 1
    return word_counter



####################################################### Start of execution


######## change filename below (any text format) ########
text_file = open("005 Histogram of Words\\README.md", "r", encoding="utf-8")

data = text_file.read()
text_file.close()
print(data)
counts = word_count(data)
# print(counts.keys())
# print(counts.values())

#n, bins, patches = plt.hist(x, 50, density=1, facecolor='g', alpha=0.75)

""" n, bins, patches = plt.scatter(labels, counts, density=1, facecolor='g', alpha=0.75)

plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title('Histogram of IQ')
#plt.text(60, .025, r'$\mu=100,\ \sigma=15$')
plt.axis([40, 160, 0, 0.03])
plt.grid(True)
plt.show()

 """

"""""
d  = {'remunerado': 20, 'funã§ã£o': 22, 'especificaã§ã£o': 3, 'talhada': 1, 'nelas': 1, 'englobados': 55, 'claramente': 1, 'finida': 1, 'contrato': 17, 'escrito': 1, 'prã©vio;': 1, 'apenas': 2}
d2 = list(filter(lambda s : d[s]>6, d))
# https://thispointer.com/python-filter-a-dictionary-by-conditions-on-keys-or-values/
d3 = {key:value for (key, value) in d.values() if len(value) == 6 }
d4 = {key:value for (key, value) in d.values() if key % 2 == 0}
d5 = {key:value for (key, value) in d.items()   if value>9 }
"""
# counts = list(filter(lambda x : counts.values() > 10))



######## What to filter out ########
MIN_CNT = 1
MIN_LEN = 2

# Do the filtering 
filt_count = {key:value for (key, value) in counts.items()   if value>MIN_CNT and len(key)>MIN_LEN }

# For ordered Histogram
filt_count = dict(sorted(filt_count.items(), key=lambda x: x[1], reverse=True))

#Prepare chart
indexes = np.arange(len(filt_count.keys()))
width = 0.7
plt.bar(indexes, filt_count.values(), width)
plt.xticks(indexes, filt_count.keys())
plt.xticks(rotation=90,ha='center')


#plt.gca().set_position([0, 0, 1, 1])
#plt.autoscale()
#plt.savefig("histogram.svg", transparent=True)

plt.autoscale()
plt.savefig("histogram.png", dpi = 300, transparent=True,bbox_inches = "tight")

plt.show()

print(filt_count)


