# Nice Histogram From Text

A small and nifty personal exercise that I thought might be useful to somebody else.

Read a TXT file and build a nice histogram, saving it in a file.

##Features

 * Reading from any text file, 
 * Filters for min_count and min_chars_of_word
 * Line 63 removes trailing "s" => simplistic rule to count singular and plural as same word

# Author:
Armando Sousa - (mailto:asousa@fe.up.pt)


## License

[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)

In short, free software: Useful, transparent, no warranty


## Tested in Windows Python 3.10

Tested tested just some workd to give a better sample result
bacuse the example file is exactly exactly this README.md


